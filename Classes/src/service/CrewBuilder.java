package service;

import java.util.ArrayList;
import java.util.List;

import model.CommanderName;
import model.Crew;

public class CrewBuilder {
	
	
	public static List<Crew> generateCrews(){
		
		List<Crew> list = new ArrayList<>();
		
		Crew c1 = generateTigerCrew();
		Crew c2 = generateSuCrew();
		Crew c3 = generateT34Crew();
		list.add(c1);
		list.add(c2);
		list.add(c3);
		
		return list;
	}
	
	
	
	public static Crew generateTigerCrew(){
		
		Crew crew = new Crew(5, (byte) 90, CommanderName.CARIUS);
		return crew;		
	}
	
	public static Crew generateSuCrew(){
		
		Crew crew = new Crew(3, (byte) 60, CommanderName.BILOTTE);
		return crew;		
	}
	
	public static Crew generateT34Crew(){
		
		Crew crew = new Crew(4, (byte) 5, CommanderName.KOLOBANOV);
		return crew;		
	}
	
}
