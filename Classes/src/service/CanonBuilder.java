package service;

import java.util.ArrayList;
import java.util.List;

import model.Canon;
import model.ShellType;

public class CanonBuilder {
	
	
	public static List<Canon> generateCanons() {
		
		List<Canon> list = new ArrayList<>();
		
		Canon c1 = generateTigerCanon();
		Canon c2 = generateSuCanon();
		Canon c3 = generateT34Canon();
		
		list.add(c1);
		list.add(c2);
		list.add(c3);
		
		return list;
	}
		
	public static Canon generateTigerCanon() {
		
		Canon c = new Canon(88, 1500, ShellType.AP);
		return c;		
	}
	
	public static Canon generateSuCanon() {
		
		Canon c = new Canon(150, 1700, ShellType.HEAT);
		return c;
	}
	
	public static Canon generateT34Canon() {
		
		Canon c = new Canon(85, 700, ShellType.HESH);
		return c;
	}

}
