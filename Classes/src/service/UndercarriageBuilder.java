package service;

import java.util.ArrayList;
import java.util.List;

import model.SuspensionType;
import model.Undercarriage;;

public class UndercarriageBuilder {
	
	
	
	public static List<Undercarriage> generateUndercarriages(){
		
		List<Undercarriage> list = new ArrayList<>();
		
		Undercarriage u1 = UndercarriageBuilder.generateTigerUndercarriage();
		Undercarriage u2 = UndercarriageBuilder.generateSuUndercarriage();
		Undercarriage u3 = UndercarriageBuilder.generateT34Undercarriage();
		
		list.add(u1);
		list.add(u2);
		list.add(u3);
		
		return list;
	}
	
	
	
	
	public static Undercarriage generateTigerUndercarriage() {
		
		Undercarriage u; 
		u = new Undercarriage(150, (float) 34.5, SuspensionType.CHRISTIE);
		return u;
	}
	
	public static Undercarriage generateSuUndercarriage() {
		
		Undercarriage u = new Undercarriage(500000, (float) 23.1, SuspensionType.HYBRID);
		return u;		
	}
	
public static Undercarriage generateT34Undercarriage() {
		
		Undercarriage u = new Undercarriage(8, (float) 200.574, SuspensionType.TRACKED);
		return u;		
	}
	
}
