package service;

import java.util.ArrayList;			// importuje liste
import java.util.List;

import model.Engine;			// model to jest zbiór klas - w tym programie mamy 2 zbiory: model i service
import model.PetrolType;		// to jest sygnalizacja, ze bede tego uzywal, i wskazuje sciezke jak dotrzec do tej klasy

public class EngineBuilder {

	public static List<Engine> generateEngines() {		// public static=metoda, typ=lista, Engine - bedzie zawierala 
														// obiekty typu Engine
		List<Engine> list = new ArrayList<>();
		
		Engine e1 = generateSuEngine();					// tworze zmienna e1= pod nią będzie konkretny silnik (obiekt)
		Engine e2 = generateTigerEngine();				// generate to jest metoda ktora jest tworzona w innej klasie
														// tj tutaj w EngineBuilder  (z CTRL klikam myszką pr.kl )
		list.add(e1);									// funkcja ktora dodaje do listy=> dlatego zaimportowalismy liste  (jest w jdk)
		list.add(e2);
		
		return list;
	}
	
	public static Engine generateSuEngine() {		// metoda generujaca silniki dla czolgow Su
		Engine e = new Engine(45, PetrolType.ON);	// uzywamy zmiennej  "e" - ta zmienna bedzie tylko w tej metodzie
		return e;
	}
	
	

	public static Engine generateTigerEngine() {
		Engine e = new Engine(0, PetrolType.BENZINE_LPG);		
		e.setPower(1900);
		return e;
	}

}
