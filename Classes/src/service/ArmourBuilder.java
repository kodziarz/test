package service;

import model.Armour;
import model.ArmourType;

public class ArmourBuilder {
	
	public static Armour generateSuArmour() {
		Armour armour = new Armour(20,30, ArmourType.CAST);		
		return armour;
	}
	
	public static Armour generateTigerArmour() {
		Armour armour = new Armour(27,52, ArmourType.RIVETED);		
		return armour;
	}
	
	public static Armour generateT34Armour() {
		Armour armour = new Armour(15,100, ArmourType.WELDED);		
		return armour;		
	}
	

}