package service;

import java.util.ArrayList;
import java.util.List;

import model.Armour;
import model.Canon;
import model.Crew;
import model.Engine;
import model.PetrolType;
import model.Tank;
import model.TankType;
import model.Undercarriage;


public class TankBuilder {
	
	public static List<Tank> generateTanks() {
		
		List<Tank> list = new ArrayList<>();
		
		Tank t34 = generateT34();
		Tank tiger = generateTiger();
		Tank su = generateSu();
		list.add(t34);
		list.add(tiger);
		list.add(su);
		
		
		return list;
	}
	
	public static Tank generateT34() {
		String name = "t-34";
		TankType type = TankType.MEDIUM;
		Engine engine = new Engine(43, PetrolType.BENZINE);
		Armour armour = ArmourBuilder.generateT34Armour();
		Undercarriage undercarriage = UndercarriageBuilder.generateT34Undercarriage();
		Canon canon = CanonBuilder.generateT34Canon();
		Crew crew = CrewBuilder.generateT34Crew();
		
		Tank tank = new Tank();
		tank.setName(name);
		tank.setTankType(type);
		tank.setVelocity(100);
		tank.setEngine(engine);
		tank.setArmour(armour);
		tank.setUndercarriage(undercarriage);
		tank.setCanon(canon);
		tank.setCrew(crew);
		
		
		return tank;
	}

	public static Tank generateTiger() {
		Engine engine = EngineBuilder.generateTigerEngine();
		Armour armour = ArmourBuilder.generateTigerArmour();
		Canon canon = CanonBuilder.generateTigerCanon();
		Crew crew = CrewBuilder.generateTigerCrew(); 
		Undercarriage undercarriage = UndercarriageBuilder.generateTigerUndercarriage();
		
		return new Tank("Tiger", TankType.HEAVY, 67, engine, armour, canon, crew, undercarriage);
	}
	

	public static Tank generateSu() {
		Engine engine = EngineBuilder.generateSuEngine();
		Armour armour = ArmourBuilder.generateSuArmour();
		Canon canon = CanonBuilder.generateSuCanon();
		Crew crew = CrewBuilder.generateSuCrew();
		Undercarriage undercarriage = UndercarriageBuilder.generateSuUndercarriage();
		
		return new Tank("Su-125", TankType.TANK_DESTROYER, 2, engine, armour, canon, crew, undercarriage );
	}

}
