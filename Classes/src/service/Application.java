package service;

import java.util.List;

import model.Tank;

public class Application {
	
	public static void main (String[] args) {
		
		
		
		//statyczne wykonanie metody
		List<Tank> tanksList = TankBuilder.generateTanks();
		
		for(Tank i : tanksList) {
			System.out.println(i);
		}

	}

}
