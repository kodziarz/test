package model;

public class Undercarriage {
	
	private int hoistingCapacity;
	private float groundClearance;
	private SuspensionType suspensionType;
	
	
	
	public Undercarriage() {
		super();
	}




	public Undercarriage(int hoistingCapacity, float groundClearance, SuspensionType suspensionType) {
		super();
		this.hoistingCapacity = hoistingCapacity;
		this.groundClearance = groundClearance;
		this.suspensionType = suspensionType;
	}



	

	public SuspensionType getSuspensionType() {
		return suspensionType;
	}




	public void setSuspensionType(SuspensionType suspensionType) {
		this.suspensionType = suspensionType;
	}




	public int getHoistingCapacity() {
		return hoistingCapacity;
	}




	public void setHoistingCapacity(int hoistingCapacity) {
		this.hoistingCapacity = hoistingCapacity;
	}




	public float getGroundClearance() {
		return groundClearance;
	}




	public void setGroundClearance(float groundClearance) {
		this.groundClearance = groundClearance;
	}




	@Override
	public String toString() {
		return "Undercarriage [hoistingCapacity=" + hoistingCapacity + ", groundClearance=" + groundClearance + "]";
	}
	
	
	
	

}
