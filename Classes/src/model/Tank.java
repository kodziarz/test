package model;

public class Tank {
	
	//pola
	private String name;
	private TankType tankType;
	private int velocity;
	private Engine engine;
	private Armour armour;
	private Canon canon;
	private Crew crew;
	private Undercarriage undercarriage;
	
	
	public Tank() {					// to jest konstruktor, ale powoluje pusty obiekt bez parametrow
	
	}

	
	
						// ponizej jest konstruktor, poniewaz
						// przypisuje wartosci do obiektu Tank roznych jego parametrow
	
	public Tank(String name, TankType tankType, int velocity, Engine engine, Armour armour, Canon canon,
			Crew crew, Undercarriage undercarriage) {
		super();
		this.name = name;
		this.tankType = tankType;
		this.velocity = velocity;
		this.engine = engine;
		this.armour = armour;
		this.canon = canon;
		this.crew = crew;
		this.undercarriage = undercarriage;
	}

	






	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public TankType getTankType() {
		return tankType;
	}



	public void setTankType(TankType tankType) {
		this.tankType = tankType;
	}



	public int getVelocity() {
		return velocity;
	}
	public void setVelocity(int velocity) {
		this.velocity = velocity;
	}

	
	
	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}
	
	

	public Armour getArmour() {
		return armour;
	}

	public void setArmour(Armour armour) {
		this.armour = armour;
	}

	public Canon getCanon() {
		return canon;
	}

	public void setCanon(Canon canon) {
		this.canon = canon;
	}

	public Crew getCrew() {
		return crew;
	}

	public void setCrew(Crew crew) {
		this.crew = crew;
	}

	public Undercarriage getUndercarriage() {
		return undercarriage;
	}

	public void setUndercarriage(Undercarriage undercarriage) {
		this.undercarriage = undercarriage;
	}



	@Override
	public String toString() {
		return "Tank [name=" + name + ", tankType=" + tankType + ", velocity=" + velocity + ", engine=" + engine
				+ ", armour=" + armour + ", canon=" + canon + ", crew=" + crew + ", undercarriage=" + undercarriage
				+ "]";
	}

	
	
	

	
		
	

}
