package model;

public enum SuspensionType {
	TRACKED,
	WHEELY,
	HYBRID,
	CHRISTIE
}
