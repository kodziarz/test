package model;

public enum TankType {
	HEAVY,
	MEDIUM,
	LIGHT,
	TANK_DESTROYER
}
