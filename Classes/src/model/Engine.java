package model;

public class Engine {
	
	private int power;
	private PetrolType petrolType;
												// konstruktor to jest metoda, ktora nazywa sie jak klasa ( najważniejsza metoda w tej klasie )
	public Engine() {							// konstruktor powoluje pusty obiekt bez parametrow	
		super();
	}

	public Engine(int power, PetrolType petrolType) {    // w metodzie by zastosowac zmienna z zewn, w nawiasach podajemy te zmienne
		super();
		this.power = power;					// dzieki temu zapisowi podczas tworzenia obiektu moge od razu podac parametry i powolac 
		this.petrolType = petrolType;		// nowy obiekt juz z konkretnymi danymi, np. z waroscia mocy i typem paliwa na jaki dziala
	}



	public int getPower() {				// metoda wyciągająca dane o mocy z klasy Engine
		return power;
	}



	public void setPower(int power) {		// metoda ustawiająca parametr power
		this.power = power;
	}



	public PetrolType getPetrolType() {		// PetrolType = typ zmiennej np.  klasa (tutaj akurat Enum). 
		return petrolType;					// Metoda by dostac dane na temat paliwa
	}



	public void setPetrolType(PetrolType petrolType) {		// metoda set, ktora ustawia rodzaj paliwa
		this.petrolType = petrolType;
	}



	@Override
	public String toString() {								// metoda toString = by dalo sie wyswietlic zmienne na ekranie
		return "Engine [power=" + power + ", petrolType=" + petrolType + "]";
	}

	

		
	
	
	
}
