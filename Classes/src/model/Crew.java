package model;

public class Crew {
	
	private int numberPeople;
	private byte condition; // w procentach
	private CommanderName commanderName;
	
	
	public Crew() {
		super();
	}


		
	
	public Crew(int numberPeople, byte condition, CommanderName commanderName) {
		super();
		this.numberPeople = numberPeople;
		this.condition = condition;
		this.commanderName = commanderName;
	}







	public int getNumberPeople() {
		return numberPeople;
	}




	public void setNumberPeople(int numberPeople) {
		this.numberPeople = numberPeople;
	}




	public CommanderName getCommanderName() {
		return commanderName;
	}




	public void setCommanderName(CommanderName commanderName) {
		this.commanderName = commanderName;
	}




	public byte getCondition() {
		return condition;
	}




	public void setCondition(byte condition) {
		this.condition = condition;
	}




	@Override
	public String toString() {
		return "Crew [numberPeople=" + numberPeople + ", condition=" + condition + ", commanderName=" + commanderName
				+ "]";
	}




	
	
	
	
	
	
	
}
