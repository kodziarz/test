package model;

public class Canon {

	private int calibre;
	private int shellRange;
	private ShellType shellType;
	
	
	
	public Canon() {
		super();
	}


	public Canon(int calibre, int shellRange, ShellType shellType) {
		super();
		this.calibre = calibre;
		this.shellRange = shellRange;
		this.shellType = shellType;
		
	}


	
	
	
	public ShellType getShellType() {
		return shellType;
	}


	public void setShellType(ShellType shellType) {
		this.shellType = shellType;
	}


	public int getCalibre() {
		return calibre;
	}


	public void setCalibre(int calibre) {
		this.calibre = calibre;
	}


	public int getShellRange() {
		return shellRange;
	}


	public void setShellRange(int shellRange) {
		this.shellRange = shellRange;
	}


	@Override
	public String toString() {
		return "Canon [calibre=" + calibre + ", shellRange=" + shellRange + ", shellType=" + shellType + "]";
	}


	
	
	
	

}
