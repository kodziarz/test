package model;

public enum ArmourType {
	CAST,
	RIVETED,
	WELDED
}
