package model;

//to jest ok
public class Armour {
	
	private float thickness;
	private int angulation;
	private ArmourType armourType;
		
	public Armour() {
		
	}

	

	public Armour(float thickness, int angulation, ArmourType armourType) {
		super();
		this.thickness = thickness;
		this.angulation = angulation;
		this.armourType = armourType;
	}






	public ArmourType getArmourType() {
		return armourType;
	}



	public void setArmourType(ArmourType armourType) {
		this.armourType = armourType;
	}


	public float getThickness() {
		return thickness;
	}




	public void setThickness(float thickness) {
		this.thickness = thickness;
	}




	public int getAngulation() {
		return angulation;
	}




	public void setAngulation(int angulation) {
		this.angulation = angulation;
	}






	@Override
	public String toString() {
		return "Armour [thickness=" + thickness + ", angulation=" + angulation + ", armourType=" + armourType + "]";
	}




	
	
	
	

	
}
